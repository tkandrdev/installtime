package com.github.andrdev.installtime;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends ActionBarActivity {
    private String mPackage;
    @InjectView(R.id.installTimeStamp)
    TextView mInstallTimeStamp;
    @InjectView(R.id.latitude)
    TextView mLatitude;
    @InjectView(R.id.longitude)
    TextView mLongitude;
    @InjectView(R.id.updateTimeStamp)
    TextView mUpdateTimeStamp;

    Date installDate;
    Date mUpdateDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPackage = getApplicationContext().getPackageName();
        ButterKnife.inject(this);
        installDate = installTimeFromPackageManager(mPackage);
        mUpdateDate = updateTimeFromPackageManager(mPackage);
        AccountManager accountManager = AccountManager.get(getApplicationContext());

        mUpdateTimeStamp.setText(getAccount(accountManager));
//        if(installDate!=null)
//            mInstallTimeStamp.setText(installDate.toString());
//        if(mUpdateDate!=null)
//            mUpdateTimeStamp.setText(mUpdateDate.toString());
//        double[] location = getGPS();
//        mLatitude.setText(String.valueOf(location[0]));
//        mLongitude.setText(String.valueOf(location[1]));
        try {
            Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                    new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, null, null, null, null);
            startActivityForResult(intent, REQUEST_CODE_EMAIL);
        } catch (ActivityNotFoundException e) {
            // TODO
        }
    }

    private double[] getGPS() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);

        Location l = null;

        for (int i=providers.size()-1; i>=0; i--) {
            l = lm.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }

        double[] gps = new double[2];
        if (l != null) {
            gps[0] = l.getLatitude();
            gps[1] = l.getLongitude();
        }
        return gps;
    }

    private Date installTimeFromPackageManager(String packageName) {
        try {
            long timestamp = getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(packageName, 0)
                    .firstInstallTime;
            return new Date(timestamp);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private Date updateTimeFromPackageManager(String packageName) {
        try {
            long timestamp = getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(packageName, 0)
                    .lastUpdateTime;
            return new Date(timestamp);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
    private static String getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccounts();
        Account account;
//        WifiManager wifiMgr;
//        wifiMgr.getConnectionInfo();


        String arra = Arrays.toString(accounts);
        return arra;
    }   private static final int REQUEST_CODE_EMAIL = 1;

    // ...



    // ...

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_EMAIL && resultCode == RESULT_OK) {
            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            mInstallTimeStamp.setText(accountName);
        }
    }
}
