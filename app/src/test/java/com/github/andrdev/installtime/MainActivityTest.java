package com.github.andrdev.installtime;

import android.app.Activity;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricGradleTestRunner.class)
@Config(manifest = "app/src/main/AndroidManifest.xml", constants = BuildConfig.class, emulateSdk = 21)
public class MainActivityTest {

    @Test
    public void onCreate_shouldInflateTheMenu() throws Exception {
        Activity activity = Robolectric.setupActivity(MainActivity.class);

        final TextView menu = (TextView)shadowOf(activity).findViewById(R.id.updateText);
        assertThat(menu.getText()).isEqualTo("Update Date:");
//        assertThat(menu.findItem(R.id.item2).getTitle()).isEqualTo("Second menu item");
    }
}
